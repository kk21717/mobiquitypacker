﻿using System;

namespace com.mobiquity.packer
{
    internal class Program
    {
        //Driver
        public static void Main(string[] args)
        {
            var filePathExample = $"{Environment.CurrentDirectory}\\..\\..\\..\\Resources\\example_input";
            Console.WriteLine($"Packing file: \"{filePathExample}\"");
            var resExample= Packer.pack(filePathExample);
            Console.WriteLine($"Output : \n{resExample}");
        }
    }
}