﻿using Microsoft.Extensions.Configuration;

namespace com.mobiquity.packer.config
{
    /// <summary>
    /// reads the appseetings.json or any other config file that be sent in ctor
    /// if can not find the config file or it is empty it will return the option
    /// values which have been set as default in AppSettings Ctor
    /// every config value can be overriden in appsettings.json and it is not mandatory
    /// </summary>
    public class SettingsService : ISettingsService
    {
        private readonly string _optionFilePath;

        private AppSettings _options;

        
        
        public SettingsService(string optionFilePath = "appsettings.json")
        {
            _optionFilePath = optionFilePath;
        }


        public AppSettings Options
        {
            get => _options ?? LoadAppSettings();
            set => _options = value;
        }


        /// <summary>
        /// load configs into an instance of AppSettings class by reading and parsing the file determined by _optionFilePath
        /// </summary>
        /// <returns></returns>
        private AppSettings LoadAppSettings()
        {
            var config = new ConfigurationBuilder().AddJsonFile(_optionFilePath, true, true).Build();
            var instance = new AppSettings();
            config.Bind(instance, c => c.BindNonPublicProperties = true);
            return instance;
        }
    }
}