﻿namespace com.mobiquity.packer.config
{
    public interface ISettingsService
    {
        /// <summary>
        /// Provide the configuration properties from the config source 
        /// </summary>
        public AppSettings Options { set; get; }
    }
}