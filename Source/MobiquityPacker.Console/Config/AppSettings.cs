﻿namespace com.mobiquity.packer.config
{
    /// <summary>
    /// a DTO to hold appsettings.js values 
    /// </summary>
    public class AppSettings
    {
        /// <summary>
        /// set default value for all properties in ctor
        /// </summary>
        public AppSettings()
        {
            // There might be up to 15 items you need to choose from
            MaxItemsCount = 15;

            // Max weight that a package can take is ≤ 100
            MaxPackageWeight = 100;

            // Max weight of an item is ≤ 100
            MaxItemWeight = 100;

            // Max cost of an item is ≤ 100
            MaxItemCost = 100;

            // As the values in example_input
            WeightDecimalPlaces = 2;

            // As in the example_output for the row that has not any item
            StrNoItems = "-";

            // In the example_input all cost values have € as their prefix
            StrCostUnit = "€";

            // Default solve strategy ( algorithm ) to be used for solving the problem
            SolverStrategy = SolverStrategyType.RecursiveStrategy;
        }


        /// <summary>
        /// Max number of items in every test case of input file
        /// </summary>
        public int MaxItemsCount { get; set; }

        /// <summary>
        /// Max weight that a package can take , default is 100
        /// </summary>
        public int MaxPackageWeight { get; set; }

        /// <summary>
        /// Max weight of an item  
        /// </summary>
        public int MaxItemWeight { get; set; }

        /// <summary>
        /// Max cost of an item is 
        /// </summary>
        public int MaxItemCost { get; set; }

        /// <summary>
        /// the number of decimal places in weight values
        /// need to be known to convert weights to integers in some part of solve
        /// it doesn't represents every value should have decimal points, but if any is decimal should not exceed this limit
        /// </summary>
        public int WeightDecimalPlaces { get; set; }


        /// <summary>
        /// what string to render if a test case (input file line) does not have any solution
        /// </summary>
        public string StrNoItems { get; set; }

        /// <summary>
        /// to parse the cost values, specify what character for price unit is used as prefix
        /// </summary>
        public string StrCostUnit { get; set; }


        /// <summary>
        /// Determine which of implemented solving strategies (algorithms) to be used
        /// </summary>
        public SolverStrategyType SolverStrategy { get; set; }

        /// <summary>
        /// For Unit Testing we need to identify two objects based on their properties values (not their references) 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object other)
        {
            var toCompareWith = other as AppSettings;
            if (toCompareWith == null)
                return false;
            return MaxItemsCount == toCompareWith.MaxItemsCount &&
                   MaxPackageWeight == toCompareWith.MaxPackageWeight &&
                   MaxItemWeight == toCompareWith.MaxItemWeight &&
                   MaxItemCost == toCompareWith.MaxItemCost &&
                   WeightDecimalPlaces == toCompareWith.WeightDecimalPlaces &&
                   StrNoItems == toCompareWith.StrNoItems &&
                   StrCostUnit == toCompareWith.StrCostUnit &&
                   SolverStrategy == toCompareWith.SolverStrategy;
        }
    }

    /// <summary>
    /// represents available (implemented) solve strategies 
    /// </summary>
    public enum SolverStrategyType
    {
        RecursiveStrategy = 1,
        RetainedRecursiveStrategy = 2,
        DynamicStrategy = 3
    }
}