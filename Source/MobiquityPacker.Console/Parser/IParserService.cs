﻿using System.Collections.Generic;
using com.mobiquity.packer.model;

namespace com.mobiquity.packer.parser
{
    public interface IParserService
    {
        public IEnumerable<InputSet> Parse(string filePath);
    }
}