﻿using System;
using System.Collections.Generic;
using System.IO;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;
using com.mobiquity.packer.util;

namespace com.mobiquity.packer.parser
{
    public class ParserService : IParserService
    {
        private readonly IInputItemUtilService _inputItemUtilService;
        private readonly IInputSetUtilService _inputSetUtilService;
        private readonly ISettingsService _settingsService;

        public ParserService(
            ISettingsService settingsService,
            IInputItemUtilService inputItemUtilService,
            IInputSetUtilService inputSetUtilService
        )
        {
            _settingsService = settingsService;
            _inputItemUtilService = inputItemUtilService;
            _inputSetUtilService = inputSetUtilService;
        }

        public IEnumerable<InputSet> Parse(string filePath)
        {
            var res = new List<InputSet>();
            try
            {
                using var fs = File.Open(filePath, FileMode.Open);
                using var bs = new BufferedStream(fs);
                using var sr = new StreamReader(bs);
                string s;
                while ((s = sr.ReadLine()) != null)
                {
                    var inputSet = new InputSet();
                    var strInputBlocks = s.Split(":");
                    inputSet.PackageCapacity = int.Parse(strInputBlocks[0]);
                    var strInputItems = strInputBlocks[1].TrimStart(' ').Split(" ");
                    foreach (var strInputItem in strInputItems)
                    {
                        var item = new InputItem();

                        var strInputItemValues = strInputItem.TrimStart('(').TrimEnd(')').Split(",");
                        item.Index = int.Parse(strInputItemValues[0]);
                        item.Weight = double.Parse(strInputItemValues[1]);
                        item.Cost = int.Parse(strInputItemValues[2]
                            .TrimStart(_settingsService.Options.StrCostUnit.ToCharArray()));

                        if (_inputItemUtilService.Validate(item))
                            inputSet.Items.Add(item);
                    }

                    if (_inputSetUtilService.Validate(inputSet))
                        res.Add(inputSet);
                }
            }
            catch (FileNotFoundException)
            {
                throw new APIException("Input file not found.");
            }
            catch (FormatException)
            {
                throw new APIException("Input file invalid content format.");
            }

            return res;
        }
    }
}