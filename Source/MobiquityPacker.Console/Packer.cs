using System;
using com.mobiquity.packer.config;
using com.mobiquity.packer.parser;
using com.mobiquity.packer.solver;
using com.mobiquity.packer.util;
using Microsoft.Extensions.DependencyInjection;

namespace com.mobiquity.packer
{
    public class Packer
    {
        /// <summary>
        /// To avoid any modification in pack() signature as in the requirement,
        /// here is the possibility to set service provider using Property Injection
        /// so let pack() method be testable
        /// </summary>
        private static IServiceProvider _provider;

        /// <summary>
        /// Our DI is implemented in getter of this property,
        /// if pack() is invoked from Program.cs , _Provider is null for the first time
        /// so it will be instantiated
        /// For Unit test we can mock ServiceProvider and/or every other service and
        /// test the pack() method
        /// </summary>
        public static IServiceProvider Provider
        {
            set => _provider = value;
            get => _provider ??= new ServiceCollection()
                .AddLogging()
                .AddSingleton<ISettingsService, SettingsService>()
                .AddSingleton<IInputItemUtilService, InputItemUtilService>()
                .AddSingleton<IInputSetUtilService, InputSetUtilService>()
                .AddSingleton<IOutputSetUtilService, OutputSetUtilService>()
                .AddSingleton<IMathUtilService, MathUtilService>()
                .AddSingleton<IParserService, ParserService>()
                .AddSingleton<ISolverService, SolverService>()
                .BuildServiceProvider();
        }


        /// <summary>
        /// This is the main requested method that gets the problem, solve it and retun the solution
        /// As in the Req: You should write a class com.mobiquity.packer.Packer with a static API method named pack.
        /// </summary>
        /// <param name="filePath">the absolute path to a test file as a String</param>
        /// <returns>the solution as a String</returns>
        public static string pack(string filePath)
        {
            var res = string.Empty;
            var parser = Provider.GetService<IParserService>();
            var outputSetUtilService = Provider.GetService<IOutputSetUtilService>();
            

            var inputSets = parser.Parse(filePath);

            foreach (var inputSet in inputSets)
            {
                // strategy ( algorithm ) to be used here can be changed in appsettings.json
                var solverStrategy = Provider.GetService<ISolverService>();
                var outputSet = solverStrategy.Solve(inputSet);
                res += $"{outputSetUtilService.GetStringOf(outputSet)}\n";
            }

            return res.TrimEnd("\n".ToCharArray());
        }
    }
}
