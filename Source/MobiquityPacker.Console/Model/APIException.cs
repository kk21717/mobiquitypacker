﻿using System;

// as mentioned in requirements :
// "Your method should throw ... "com.mobiquity.packer.APIException" where relevant... ",
// I do not add the current folder at the end of namespace and do not change the name ( to adapt my code style ) 
namespace com.mobiquity.packer
{
    public class APIException : Exception
    {
        public APIException(string message) : base(message){}
    }
}