﻿using System.Collections.Generic;
using System.Linq;

namespace com.mobiquity.packer.model
{
    /// <summary>
    /// a DTO represents a solution to the problem (every line of the output string)
    /// in current implementation only a comma separated string of Items.Index will be rendered
    /// </summary>
    public class OutputSet
    {
        public OutputSet()
        {
            Items = new List<InputItem>();
            SortedIndices = new List<int>();
        }

        public int SumCost { get; set; }
        public List<InputItem> Items { get; set; }

        public List<int> SortedIndices { get; set; }

        /// <summary>
        /// provides equity comparision for OutputSet objects based on SumCost and Items Collection
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object other)
        {
            var toCompareWith = (OutputSet)other;
            if (toCompareWith == null)
                return false;
            var res = SumCost == toCompareWith.SumCost &&
                      Items.Count == toCompareWith.Items.Count;

            if (!res)
                return false;

            return !Items.Where((t, i) => !t.Equals(toCompareWith.Items[i])).Any();
        }
    }
}