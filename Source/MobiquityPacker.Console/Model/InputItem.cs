﻿namespace com.mobiquity.packer.model
{
    
    /// <summary>
    /// a DTO that represents every item to be put in the package
    /// </summary>
    public class InputItem
    {
        public InputItem(){}

        public InputItem(int index, double weight, int cost)
        {
            Index = index;
            Weight = weight;
            Cost = cost;
        }

        public int Index { get; set; }

        public double Weight { get; set; }

        public int Cost { get; set; }

        public override bool Equals(object other)
        {
            var toCompareWith = (InputItem) other;
            if (toCompareWith == null)
                return false;
            var res = Index == toCompareWith.Index &&
                      Weight == toCompareWith.Weight &&
                      Cost == toCompareWith.Cost;
            return res;
        }
    }
}