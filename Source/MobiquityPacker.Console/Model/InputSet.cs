﻿using System.Collections.Generic;
using System.Linq;

namespace com.mobiquity.packer.model
{
    /// <summary>
    /// a DTO representing every line of input file
    /// </summary>
    public class InputSet
    {
        public InputSet() => Items = new List<InputItem>();

        public double PackageCapacity { get; set; }

        public List<InputItem> Items { get; set; }

        public override bool Equals(object other)
        {
            var toCompareWith = (InputSet) other;
            if (toCompareWith == null)
                return false;
            var res = PackageCapacity == toCompareWith.PackageCapacity &&
                      Items.Count == toCompareWith.Items.Count;

            if (!res)
                return false;

            return !Items.Where((t, i) => !t.Equals(toCompareWith.Items[i])).Any();
        }
    }
}