﻿using com.mobiquity.packer.model;

namespace com.mobiquity.packer.solver
{
    public interface ISolverService
    {
        public OutputSet Solve(InputSet inputSet);
    }
}