﻿using com.mobiquity.packer.config;
using com.mobiquity.packer.model;
using com.mobiquity.packer.strategy;
using com.mobiquity.packer.Strategy;
using com.mobiquity.packer.util;

namespace com.mobiquity.packer.solver
{
    public class SolverService : ISolverService
    {
        private readonly ISolveStrategy _solveStrategy;

        /// <summary>
        /// switches between solve strategies based on the options
        /// </summary>
        /// <param name="settingsService"></param>
        /// <param name="mathUtilService"></param>
        /// <param name="inputSetUtilService"></param>
        public SolverService(
            ISettingsService settingsService,
            IMathUtilService mathUtilService,
            IInputSetUtilService inputSetUtilService
        )
        {
            _solveStrategy = settingsService.Options.SolverStrategy switch
            {
                SolverStrategyType.RecursiveStrategy => SolveStrategyFactory.GetRecursiveStrategy(),
                SolverStrategyType.RetainedRecursiveStrategy => SolveStrategyFactory.GetRetainedRecursiveStrategy(
                    mathUtilService),
                SolverStrategyType.DynamicStrategy => SolveStrategyFactory.GetDynamicStrategy(mathUtilService,
                    inputSetUtilService),
                _ => _solveStrategy
            };
        }

        public OutputSet Solve(InputSet inputSet) => _solveStrategy.Solve(inputSet);
        
    }
}