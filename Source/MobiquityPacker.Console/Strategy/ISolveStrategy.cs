﻿using com.mobiquity.packer.model;

namespace com.mobiquity.packer.strategy
{
    /// <summary>
    /// an interface dictating the signature for solve operation
    /// implemented as the common interface in strategy design pattern
    /// </summary>
    public interface ISolveStrategy
    {
        public OutputSet Solve(InputSet inputSet);
    }
}