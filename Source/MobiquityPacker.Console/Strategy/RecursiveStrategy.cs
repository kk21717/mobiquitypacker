﻿using System.Linq;
using com.mobiquity.packer.model;

namespace com.mobiquity.packer.strategy
{
    /// <summary>
    /// Recursive algorithm for solving the problem
    /// </summary>
    public class RecursiveStrategy : ISolveStrategy
    {
        private int[] _arrCosts;
        private double[] _arrWeights;


        public OutputSet Solve(InputSet inputSet)
        {
            // we will need to sort items by their weight ascending to meet this req:
            // You would prefer to send a package which weighs less in case there is more than one package with the same price.
            var sortedItems = inputSet.Items.OrderBy(x => x.Weight);

            //prepare a separated arrays for weights and costs
            _arrWeights = sortedItems.Select(x => x.Weight).ToArray();
            _arrCosts = sortedItems.Select(x => x.Cost).ToArray();

            // The main operation to solve the problem
            var outputSet = RecursiveSearch(inputSet.PackageCapacity, inputSet.Items.Count);

            // We need to re-identify the solution items by recalling solution.sortedIndices 
            foreach (var sortedIndex in outputSet.SortedIndices)
                outputSet.Items.Add(sortedItems.ElementAt(sortedIndex));

            //sort items by item index
            outputSet.Items = outputSet.Items.OrderBy(x => x.Index).ToList();

            return outputSet;
        }

        // Returns the maximum value that can be put in the pack of package weight limit W
        private OutputSet RecursiveSearch(double W, int n)
        {
            var outputSet = new OutputSet();

            // Base Case (return 0 SumCost)
            if (n == 0 || W == 0) return outputSet;


            // If weight of the nth item is more than package weight limit,
            // then this item cannot be included in the optimal outputSet
            if (_arrWeights[n - 1] > W) return RecursiveSearch(W, n - 1);


            // outputSet 1: nth item included
            var sol1 = RecursiveSearch(W - _arrWeights[n - 1], n - 1);
            sol1.SumCost += _arrCosts[n - 1];
            sol1.SortedIndices.Add(n - 1);

            // outputSet 2: nth item not included
            var sol2 = RecursiveSearch(W, n - 1);


            return sol1.SumCost > sol2.SumCost ? sol1 : sol2;
            
        }
    }
}