﻿using System.Linq;
using com.mobiquity.packer.model;
using com.mobiquity.packer.util;

namespace com.mobiquity.packer.strategy
{
    /// <summary>
    /// extended recursive algorithm which try to retain calculated paths in recursive process by employing a dynamic 2D matrix
    /// </summary>
    public class RetainedRecursiveStrategy : ISolveStrategy
    {
        private int[] _arrCosts;
        private int[] _arrWeights;

        private readonly IMathUtilService _mathUtilService;

        public RetainedRecursiveStrategy(
            IMathUtilService mathUtilService
        ) => _mathUtilService = mathUtilService;

        public OutputSet Solve(InputSet inputSet)
        {
            // we will need to sort items by their weight ascending to meet this req:
            // You would prefer to send a package which weighs less in case there is more than one package with the same price.
            var sortedItems = inputSet.Items.OrderBy(x => x.Weight);

            //prepare a separated arrays for weights and costs
            _arrWeights = sortedItems.Select(x => _mathUtilService.ScaleToInteger(x.Weight)).ToArray();
            _arrCosts = sortedItems.Select(x => x.Cost).ToArray();

            // The main operation to solve the problem
            var outputSet = Retain(_mathUtilService.ScaleToInteger(inputSet.PackageCapacity), inputSet.Items.Count);

            // We need to re-identify the solution items by recalling solution.sortedIndices 
            foreach (var sortedIndex in outputSet.SortedIndices)
                outputSet.Items.Add(sortedItems.ElementAt(sortedIndex));

            //sort items by item index
            outputSet.Items = outputSet.Items.OrderBy(x => x.Index).ToList();

            return outputSet;
        }

        // Returns the _arrCosts of maximum profit
        private OutputSet RecursiveSearch(int w, int n, int[,] dp)
        {
            var outputSet = new OutputSet();

            // Base condition
            if (n == 0 || w == 0)
                return outputSet;

            if (dp[n, w] != -1)
            {
                outputSet.SumCost = dp[n, w];
                return outputSet;
            }

            if (_arrWeights[n - 1] > w)
            {
                // Store the Costs array and stack in table before return
                var tempOutputSet = RecursiveSearch(w, n - 1, dp);
                dp[n, w] = tempOutputSet.SumCost;
                return tempOutputSet;
            }


            // outputSet 1: nth item included
            var sol1 = RecursiveSearch(w - _arrWeights[n - 1], n - 1, dp);
            sol1.SumCost += _arrCosts[n - 1];
            sol1.SortedIndices.Add(n - 1);

            // outputSet 2: nth item not included
            var sol2 = RecursiveSearch(w, n - 1, dp);


            return sol1.SumCost > sol2.SumCost ? sol1 : sol2;
        }

        private OutputSet Retain(int w, int n)
        {
            // Declare the table dynamically
            var dp = new int[n + 1, w + 1];

            // Loop to initially filled the
            // table with -1
            for (var i = 0; i < n + 1; i++)
            for (var j = 0; j < w + 1; j++)
                dp[i, j] = -1;

            return RecursiveSearch(w, n, dp);
        }
    }
}