﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.mobiquity.packer.model;
using com.mobiquity.packer.util;

namespace com.mobiquity.packer.strategy
{
    /// <summary>
    /// Algorithm based on dynamic programming , it creates a 2d table of (n+1 * w+1)
    /// where n is the number of items and w is the package weight limit determined by the input
    /// </summary>
    public class DynamicStrategy : ISolveStrategy
    {
        private readonly IInputSetUtilService _inputSetUtilService;
        private readonly IMathUtilService _mathUtilService;

        private int[] _arrCosts;
        private int[] _arrWeights;

        /// <summary>
        /// since this algorithm is not recursive, we can not send the selected items array in the result to pop up and be maintained in recursive process
        /// we use an independent array (_listSortedIndices) to be filled with the items after the main operation is done and max possible cost of package identified
        /// </summary>
        private List<int> _listSortedIndices;

        public DynamicStrategy(
            IMathUtilService mathUtilService,
            IInputSetUtilService inputSetUtilService
        )
        {
            _mathUtilService = mathUtilService;
            _inputSetUtilService = inputSetUtilService;
        }

        public OutputSet Solve(InputSet inputSet)
        {
            // we will need to sort items by their weight ascending to meet this req:
            // You would prefer to send a package which weighs less in case there is more than one package with the same price.
            var sortedItems = inputSet.Items.OrderBy(x => x.Weight);

            // prepare a separated arrays for weights and costs ( we have to transform all weight-type values from double to integer number 
            // because this algorithm creates a table based on Weight values and wee need it to be integer
            _arrWeights = _inputSetUtilService.AnyItemWithDecimalWeight(inputSet)
                ? sortedItems.Select(x => _mathUtilService.ScaleToInteger(x.Weight)).ToArray()
                : sortedItems.Select(x => Convert.ToInt32(x.Weight)).ToArray();

            _arrCosts = sortedItems.Select(x => x.Cost).ToArray();

            // the main operation of solving the problem
            var outputSet = new OutputSet
            {
                // If any item has a decimal weight value we will need to scale Package Weight Limit same as item's weight values
                // if not , simple conversion from double to int would be enough    
                SumCost = DynamicSolve(
                    _inputSetUtilService.AnyItemWithDecimalWeight(inputSet)? 
                        _mathUtilService.ScaleToInteger(inputSet.PackageCapacity): 
                        Convert.ToInt32(inputSet.PackageCapacity),
                    inputSet.Items.Count)
            };

            // We need to re-identify the solution items by recalling solution.sortedIndices 
            foreach (var sortedIndex in _listSortedIndices) outputSet.Items.Add(item: sortedItems.ElementAt(sortedIndex));

            //sort items by item index
            outputSet.Items = outputSet.Items.OrderBy(x => x.Index).ToList();

            return outputSet;
        }

        public int DynamicSolve(int W, int n)
        {
            int i, w;
            var k = new int[n + 1, W + 1];

            // Build table K[][] in bottom up manner
            for (i = 0; i <= n; i++)
            for (w = 0; w <= W; w++)
                if (i == 0 || w == 0)
                    k[i, w] = 0;
                else if (_arrWeights[i - 1] > w)
                    k[i, w] = k[i - 1, w];
                else
                    k[i, w] = Math.Max(_arrCosts[i - 1] + k[i - 1, w - _arrWeights[i - 1]],k[i - 1, w]);


            // we know the max possible cost of items ( k[n, W] )
            // Now we need to get list of items that had been resulted in this solution

            var res = k[n, W];
            w = W;
            _listSortedIndices = new List<int>();

            for (i = n; i > 0 && res > 0; i--)
                // either the result comes from the top (K[i-1][w]) or from (val[i-1] + K[i-1] [w-wt[i-1]]) as in K table.
                // If it comes from the latter one/ it means the item is included.
                if (res != k[i - 1, w])
                {
                    // This item is included.
                    _listSortedIndices.Add(i - 1);

                    // Since this weight is included its value is deducted
                    res -= _arrCosts[i - 1];
                    w -= _arrWeights[i - 1];
                }

            

            return k[n, W];
        }
    }
}