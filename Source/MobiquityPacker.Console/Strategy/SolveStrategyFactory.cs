﻿using com.mobiquity.packer.strategy;
using com.mobiquity.packer.util;

namespace com.mobiquity.packer.Strategy
{
    /// <summary>
    /// A factory class containing 3 factory methods to
    /// decouple instantiation logic from using/calling strategy classes
    /// </summary>
    public class SolveStrategyFactory
    {

        public static RecursiveStrategy GetRecursiveStrategy()
            => new RecursiveStrategy();

        public static RetainedRecursiveStrategy GetRetainedRecursiveStrategy(
            IMathUtilService mathUtilService) 
            => new RetainedRecursiveStrategy(mathUtilService);
        

        public static DynamicStrategy GetDynamicStrategy(
            IMathUtilService mathUtilService,
            IInputSetUtilService inputSetUtilService)
            => new DynamicStrategy(mathUtilService, inputSetUtilService);
    }
}