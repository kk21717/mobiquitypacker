﻿using com.mobiquity.packer.model;

namespace com.mobiquity.packer.util
{
    public interface IInputItemUtilService
    {
        public bool Validate(InputItem item);
    }
}