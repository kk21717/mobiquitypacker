﻿using com.mobiquity.packer.config;
using com.mobiquity.packer.model;

namespace com.mobiquity.packer.util
{
    /// <summary>
    ///  A simple class to serve functionalists to be applied on InputItem dto
    ///  was separated from dto itself to be easily testable and access to other
    ///  injected services like settings ...
    /// </summary>
    public class InputItemUtilService : IInputItemUtilService
    {
        private readonly ISettingsService _settingsService;

        public InputItemUtilService(ISettingsService settingsService)
            => _settingsService = settingsService;

        public bool Validate(InputItem item)
        {
            if (item.Weight > _settingsService.Options.MaxItemWeight)
                throw new APIException($"MaxItemWeight ({_settingsService.Options.MaxItemWeight}) exceeded.");

            if (item.Cost > _settingsService.Options.MaxItemCost)
                throw new APIException($"MaxItemCost ({_settingsService.Options.MaxItemCost}) exceeded.");

            return true;
        }
    }
}