﻿using System.Linq;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;

namespace com.mobiquity.packer.util
{
    /// <summary>
    ///  A simple class to serve functionalists to be applied on OutputSet dto
    ///  was separated from dto itself to be easily testable and access to other
    ///  injected services like settings ...
    /// </summary>
    public class OutputSetUtilService : IOutputSetUtilService
    {
        private readonly ISettingsService _settingsService;

        public OutputSetUtilService(ISettingsService settingsService)
            => _settingsService = settingsService;

        public string GetStringOf(OutputSet outputSet)
        {
            return outputSet.Items.Any()
                ? string.Join(',', outputSet.Items.OrderBy(x => x.Index).Select(x => x.Index))
                : _settingsService.Options.StrNoItems;
        }
    }
}