﻿using com.mobiquity.packer.model;

namespace com.mobiquity.packer.util
{
    public interface IOutputSetUtilService
    {
        public string GetStringOf(OutputSet outputSet);
    }
}