﻿namespace com.mobiquity.packer.util
{
    public interface IMathUtilService
    {
        public int ScaleToInteger(double value);
    }
}