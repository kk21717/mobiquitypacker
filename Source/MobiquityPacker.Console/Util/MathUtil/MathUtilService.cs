﻿using System;
using com.mobiquity.packer.config;

namespace com.mobiquity.packer.util
{
    /// <summary>
    ///  A class to serve required mathematical functionalists 
    /// </summary>
    public class MathUtilService : IMathUtilService
    {
        private readonly ISettingsService _settingsService;

        public MathUtilService(ISettingsService settingsService)
            => _settingsService = settingsService;

        public int ScaleToInteger(double value)
            => Convert.ToInt32(Math.Pow(10, _settingsService.Options.WeightDecimalPlaces) * value);

    }
}