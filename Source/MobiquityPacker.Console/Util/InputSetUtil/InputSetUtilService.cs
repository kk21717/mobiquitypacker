﻿using System;
using System.Linq;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;

namespace com.mobiquity.packer.util
{
    /// <summary>
    ///  A simple class to serve functionalists to be applied on InputSet dto
    ///  was separated from dto itself to be easily testable and access to other
    ///  injected services like settings ...
    /// </summary>
    public class InputSetUtilService : IInputSetUtilService
    {
        private readonly ISettingsService _settingsService;

        public InputSetUtilService(ISettingsService settingsService)
            => _settingsService = settingsService;

        public bool Validate(InputSet inputSet)
        {
            if (inputSet.PackageCapacity > _settingsService.Options.MaxPackageWeight)
                throw new APIException($"MaxPackageWeight ({_settingsService.Options.MaxPackageWeight}) exceeded.");

            if (inputSet.Items.Count > _settingsService.Options.MaxItemsCount)
                throw new APIException($"MaxItemsCount ({_settingsService.Options.MaxItemsCount}) exceeded.");

            return true;
        }

        public bool AnyItemWithDecimalWeight(InputSet inputSet)
            => inputSet.Items.Any(item => Math.Abs(item.Weight - Math.Floor(item.Weight)) > 0);
    }
}