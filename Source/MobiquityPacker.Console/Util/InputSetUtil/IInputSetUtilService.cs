﻿using com.mobiquity.packer.model;

namespace com.mobiquity.packer.util
{
    public interface IInputSetUtilService
    {
        public bool Validate(InputSet inputSet);
        public bool AnyItemWithDecimalWeight(InputSet inputSet);
    }
}