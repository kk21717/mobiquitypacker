﻿using System;
using System.Collections.Generic;
using System.Text;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;
using com.mobiquity.packer.parser;
using com.mobiquity.packer.strategy;
using com.mobiquity.packer.util;
using NUnit.Framework;

namespace MobiquityPacker.Console.Tests.Strategy
{
    [TestFixture]
    public class DynamicStrategyTests
    {
        [SetUp]
        public void SetUp() { }

        [TestCase]
        public void Solve_SimpleSet()
        {
            //Arrange
            var settingsService = new SettingsService();
            var dynamicStrategy = new DynamicStrategy(
                new MathUtilService(settingsService), new InputSetUtilService(settingsService));
            var inputSet = new InputSet()
            {
                PackageCapacity = 10,
                Items =
                {
                    new InputItem(1, 3, 7),
                    new InputItem(2, 8, 8),
                    new InputItem(3, 6, 4)
                }
            };
            // "1,3";
            var expectedResult = new OutputSet()
            {
                SumCost = 11,
                Items = new List<InputItem>()
                {
                    new InputItem(1, 3, 7),
                    new InputItem(3, 6, 4)
                }
            };
            
            //Act
            var result = dynamicStrategy.Solve(inputSet);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void Solve_DuplicateSolutionWithSamePrice()
        {
            //Arrange
            var settingsService = new SettingsService();
            var dynamicStrategy = new DynamicStrategy(
                new MathUtilService(settingsService), new InputSetUtilService(settingsService));
            var inputSet = new InputSet()
            {
                PackageCapacity = 10,
                Items =
                {
                    new InputItem(1, 1, 20),
                    new InputItem(2, 2, 5),
                    new InputItem(3, 3, 10),
                    new InputItem(4, 2.9, 10),
                    new InputItem(5, 7, 15),
                    new InputItem(6, 4, 25)
                }
            };
            // "1,2,4,6";
            var expectedResult = new OutputSet()
            {
                SumCost = 60,
                Items = new List<InputItem>()
                {
                    new InputItem(1, 1, 20),
                    new InputItem(2, 2, 5),
                    new InputItem(4, 2.9, 10),
                    new InputItem(6, 4, 25)
                }
            };

            //Act
            var result = dynamicStrategy.Solve(inputSet);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void Solve_MultiSolutionWithSamePrice()
        {
            //Arrange
            var settingsService = new SettingsService();
            var dynamicStrategy = new DynamicStrategy(
                new MathUtilService(settingsService), new InputSetUtilService(settingsService));
            var inputSet = new InputSet()
            {
                PackageCapacity = 10,
                Items =
                {
                    new InputItem(1, 4, 20),
                    new InputItem(2, 2, 10),
                    new InputItem(3, 1.8, 10),
                    new InputItem(4, 1.9, 10),
                    new InputItem(5, 15, 10),
                    new InputItem(6, 4, 25)
                }
            };
            // "1,3,6";
            var expectedResult = new OutputSet()
            {
                SumCost = 55,
                Items = new List<InputItem>()
                {
                    new InputItem(2, 2, 10),
                    new InputItem(3, 1.8, 10),
                    new InputItem(4, 1.9, 10),
                    new InputItem(6, 4, 25)
                }
            };

            //Act
            var result = dynamicStrategy.Solve(inputSet);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }
    }
}
