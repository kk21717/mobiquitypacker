﻿using com.mobiquity.packer;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;
using com.mobiquity.packer.parser;
using com.mobiquity.packer.util;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace MobiquityPacker.Console.Tests.Parser
{
    [TestFixture]
    public class ParserServiceTests
    {

        private readonly string _basePathConsoleTestsDev = Environment.CurrentDirectory + "/../../..";

        [SetUp]
        public void SetUp(){}

        [TestCase]
        public void parse_SimpleFile() {
            //Arrange
            var mockSettingsService = new SettingsService(); 
            var mockInputItemUtilService = new InputItemUtilService(mockSettingsService);  
            var mockInputSetUtilService = new InputSetUtilService(mockSettingsService); 

            var parserService = new ParserService(mockSettingsService, mockInputItemUtilService, mockInputSetUtilService);
            var filePath = $"{_basePathConsoleTestsDev}/Parser/Resources/SimpleFile/input"; //81 : (1, 53.38,€45) (2, 88.62,€98)
            var expectedResult = new List<InputSet>
            {
                new InputSet()
                {
                    PackageCapacity = 81, 
                    Items = {
                        new InputItem(1, 53.38, 45), 
                        new InputItem(2, 88.62, 98)
                    }
                }
            };

            //Act
            var result = parserService.Parse(filePath);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void parse_InvalidFormat() {
            //Arrange
            var mockSettingsService = new SettingsService();
            var mockInputItemUtilService = new InputItemUtilService(mockSettingsService);
            var mockInputSetUtilService = new InputSetUtilService(mockSettingsService);

            var parserService = new ParserService(mockSettingsService, mockInputItemUtilService, mockInputSetUtilService);
            var filePath = $"{_basePathConsoleTestsDev}/Parser/Resources/InvalidFormat/input"; //THIS_IS_INVALID_CONTENT_FORMAT
            var expectedExceptionMessage = "Input file invalid content format.";

            // Assert
            var ex = Assert.Throws<APIException>(() => { 
                //Act
                var result = parserService.Parse(filePath); 
            });
            Assert.That(ex?.Message, Is.EqualTo(expectedExceptionMessage));
        }

        [TestCase]
        public void parse_FileNotFound() {
            //Arrange
            var mockSettingsService = new SettingsService();
            var mockInputItemUtilService = new InputItemUtilService(mockSettingsService);
            var mockInputSetUtilService = new InputSetUtilService(mockSettingsService);

            var parserService = new ParserService(mockSettingsService, mockInputItemUtilService, mockInputSetUtilService);
            const string expectedExceptionMessage = "Input file not found.";
            var filePath = $"NOT_EXISTING_PATH"; 
            
            // Assert
            var ex = Assert.Throws<APIException>(() => {
                //Act
                var result = parserService.Parse(filePath);
            });
            Assert.That(ex?.Message, Is.EqualTo(expectedExceptionMessage));
        }

        
    }
}
