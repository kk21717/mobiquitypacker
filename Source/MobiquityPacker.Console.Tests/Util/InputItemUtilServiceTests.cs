﻿using System;
using System.Collections.Generic;
using System.Text;
using com.mobiquity.packer;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;
using com.mobiquity.packer.strategy;
using com.mobiquity.packer.util;
using NUnit.Framework;

namespace MobiquityPacker.Console.Tests.Util
{
    [TestFixture]
    public class InputItemUtilServiceTests
    {
        //[SetUp]
        //public void SetUp() { }

        [TestCase]
        public void Validate_MaxItemWeight_Pass()
        {
            //Arrange
            var inputItemUtilService = new InputItemUtilService(new SettingsService());
            const int maxValue = 100;
            var inputItem = new InputItem(1, maxValue , 1);
            const bool expectedResult = true;

            //Act
            var result = inputItemUtilService.Validate(inputItem);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void Validate_MaxItemWeight_Violate()
        {
            //Arrange
            var inputItemUtilService = new InputItemUtilService(new SettingsService());
            const int maxValue = 100;
            var inputItem = new InputItem(1, maxValue +1, 1);


            // Assert
            var ex = Assert.Throws<APIException>(() =>
            {
                // Act
                var result = inputItemUtilService.Validate(inputItem);
            });


        }

        [TestCase]
        public void Validate_MaxItemCost_Pass()
        {
            //Arrange
            var inputItemUtilService = new InputItemUtilService(new SettingsService());
            const int maxValue = 100;
            var inputItem = new InputItem(1, 1, maxValue);
            const bool expectedResult = true;

            //Act
            var result = inputItemUtilService.Validate(inputItem);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void Validate_MaxItemCost_Violate()
        {
            //Arrange
            var inputItemUtilService = new InputItemUtilService(new SettingsService());
            const int maxValue = 100;
            var inputItem = new InputItem(1, 1, maxValue + 1);


            // Assert
            var ex = Assert.Throws<APIException>(() =>
            {
                // Act
                var result = inputItemUtilService.Validate(inputItem);
            });


        }
    }
}
