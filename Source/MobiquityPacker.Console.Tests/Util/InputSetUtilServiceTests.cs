﻿using System;
using System.Collections.Generic;
using System.Text;
using com.mobiquity.packer;
using com.mobiquity.packer.config;
using com.mobiquity.packer.model;
using com.mobiquity.packer.strategy;
using com.mobiquity.packer.util;
using NUnit.Framework;

namespace MobiquityPacker.Console.Tests.Util
{
    [TestFixture]
    public class InputSetUtilServiceTests
    {
        //[SetUp]
        //public void SetUp() { }

        [TestCase]
        public void Validate_MaxPackageWeight_Pass()
        {
            //Arrange
            var inputSetUtilService = new InputSetUtilService(new SettingsService());
            const int maxValue = 100;
            var inputSet = new InputSet()
            {
                PackageCapacity = maxValue,
                Items = new List<InputItem>()
                {
                    new InputItem(1,50,1),
                    new InputItem(2,50,1),
                }
            };
            const bool expectedResult = true;

            //Act
            var result = inputSetUtilService.Validate(inputSet);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void Validate_MaxPackageWeight_Violate()
        {
            //Arrange
            var inputSetUtilService = new InputSetUtilService(new SettingsService());
            const int maxValue = 100;
            var inputSet = new InputSet()
            {
                PackageCapacity = maxValue + 1,
                Items = new List<InputItem>()
                {
                    new InputItem(1,51,1),
                    new InputItem(2,50,1),
                }
            };


            // Assert
            var ex = Assert.Throws<APIException>(() =>
            {
                // Act
                var result = inputSetUtilService.Validate(inputSet);
            });

        }

        [TestCase]
        public void Validate_MaxItemsCount_Pass()
        {
            //Arrange
            var inputSetUtilService = new InputSetUtilService(new SettingsService());
            const int maxValue = 15;
            var inputSet = new InputSet {PackageCapacity = maxValue};
            for (var i = 1; i <= maxValue; i++)
                inputSet.Items.Add(new InputItem(i, 1, 1));
            const bool expectedResult = true;

            //Act
            var result = inputSetUtilService.Validate(inputSet);

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void Validate_MaxItemsCount_Violate()
        {
            //Arrange
            var inputSetUtilService = new InputSetUtilService(new SettingsService());
            const int maxValue = 15;
            var inputSet = new InputSet { PackageCapacity = maxValue + 1};
            for (var i = 1; i <= maxValue + 1; i++)
                inputSet.Items.Add(new InputItem(i, 1, 1));

            // Assert
            var ex = Assert.Throws<APIException>(() =>
            {
                // Act
                var result = inputSetUtilService.Validate(inputSet);
            });
        }
    }
}
