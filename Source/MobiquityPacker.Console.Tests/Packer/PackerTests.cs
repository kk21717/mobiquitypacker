﻿using System;
using System.IO;
using com.mobiquity.packer;
using NUnit.Framework;

namespace MobiquityPacker.Console.Tests
{
    [TestFixture]
    public class PackerTests
    {
        [SetUp]
        public void SetUp(){}

        private readonly string _basePathConsoleTestsDev =
            $"{Environment.CurrentDirectory}/../../..\\Packer\\Resources";

        [TestCase("given example")]
        [TestCase("single line")]
        [TestCase("multi line")]
        //Packaging Challenge > Introduction > "You would prefer to send a package which weighs less in case there is more than one package with the same price."
        [TestCase("duplicate price")]
        public void Correctness(string testFolderName)
        {
            // Arrange
            var path = $"{_basePathConsoleTestsDev}\\Correctness\\{testFolderName}\\";
            var expectedResult = File.ReadAllText(path + "output").Replace("\r\n", "\n");

            // Act
            var result = Packer.pack(path + "input");

            // Assert
            Assert.AreEqual(expectedResult, result);
        }


        //Packaging Challenge > Constraints > "Max weight that a package can take is ≤ 100"
        [TestCase("package wight limit validation", "MaxPackageWeight (100) exceeded.")]
        //Packaging Challenge > Constraints > "There might be up to 15 items you need to choose from"
        [TestCase("item count limit validation", "MaxItemsCount (15) exceeded.")]
        //Packaging Challenge > Constraints > "Max weight and cost of an item is ≤ 100"
        [TestCase("item wight limit validation", "MaxItemWeight (100) exceeded.")]
        [TestCase("item cost limit validation", "MaxItemCost (100) exceeded.")]
        [TestCase("input file not found", "Input file not found.")]
        [TestCase("input file invalid content format", "Input file invalid content format.")]
        public void Constraints(string testFolderName, string expectedExceptionMessage)
        {
            // Arrange
            var path = $"{_basePathConsoleTestsDev}\\Constraints\\{testFolderName}\\";

            // Assert
            var ex = Assert.Throws<APIException>(() =>
            {
                // Act
                Packer.pack(path + "input");
            });
            Assert.That(ex?.Message, Is.EqualTo(expectedExceptionMessage));
        }
    }
}