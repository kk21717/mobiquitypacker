﻿using System;
using com.mobiquity.packer.config;
using NUnit.Framework;

namespace MobiquityPacker.Console.Tests.Config
{
    [TestFixture]
    public class SettingsServiceTests
    {
        readonly string _basePathConsoleTestsDev = Environment.CurrentDirectory + "/../../..";

        [SetUp]
        public void SetUp(){}

        [TestCase]
        public void LoadAppSettings_AllFieldsAreSet()
        {
            //Arrange
            var settingsService = new SettingsService($"{_basePathConsoleTestsDev}/Config/Resources/AllFieldsAreSet/appsettings.json");
            var expectedResult = new AppSettings()
            {
                MaxItemsCount= 100 ,
                MaxPackageWeight = 200,
                MaxItemWeight = 300,
                MaxItemCost = 400,
                WeightDecimalPlaces = 500,
                StrNoItems = "X",
                StrCostUnit = "Y",
                SolverStrategy = SolverStrategyType.DynamicStrategy
            };

            
            //Act
            var result = settingsService.Options;

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void LoadAppSettings_SomeFieldsAreSet()
        {
            //Arrange
            var settingsService = new SettingsService($"{_basePathConsoleTestsDev}/Config/Resources/SomeFieldsAreSet/appsettings.json");
            var expectedResult = new AppSettings()
            {
                MaxItemsCount = 1000,
                StrNoItems = "SAMPLE_VALUE"
            };


            //Act
            var result = settingsService.Options;

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void LoadAppSettings_NoneOfFieldsAreSet()
        {
            //Arrange
            var settingsService = new SettingsService($"{_basePathConsoleTestsDev}/Config/Resources/NoneOfFieldsAreSet/appsettings.json");
            var expectedResult = new AppSettings();

            //Act
            var result = settingsService.Options;

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        [TestCase]
        public void LoadAppSettings_FileNotFound()
        {
            //Arrange
            var settingsService = new SettingsService("some_path_that_does_not_exists");
            var expectedResult = new AppSettings();

            //Act
            var result = settingsService.Options;

            // Assert
            Assert.AreEqual(expectedResult, result);
        }

        
    }
}
